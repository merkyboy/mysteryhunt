<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1.0001, minimum-scale=1.0001, maximum-scale=1.0001, user-scalable=no">
    <meta name="robots" content="noindex,nofollow">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="shortcut icon" href="images/emerson.ico">
    <title>Emerson</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <div class="screen1 centered text-center form-inline" id="screen1">
    	<input class="form-control input-lg txtScr1" type="text" name="txtScr1a" id="txtScr1a" maxlength="1" />
    	<input class="form-control input-lg txtScr1" type="text" name="txtScr1b" id="txtScr1b" maxlength="1" />
    	<input class="form-control input-lg txtScr1" type="text" name="txtScr1c" id="txtScr1c" maxlength="1" />
    	<input class="form-control input-lg txtScr1" type="text" name="txtScr1d" id="txtScr1d" maxlength="1" />
    	<input class="form-control input-lg txtScr1" type="text" name="txtScr1e" id="txtScr1e" maxlength="1" />
    	<input class="form-control input-lg txtScr1" type="text" name="txtScr1f" id="txtScr1f" maxlength="1" /><br />
    	<button class="btn btn-primary btn-lg" type="button" name="btnScr1" id="btnScr1">CONTINUE</button><br />
    	<span class="error" id="screen1error"></span>
    </div>
    <div class="screen2 centered text-center form-inline hide" id="screen2"></div>
    <div class="screen3 centered text-center hide" id="screen3"></div>
    
    <div class="process hide" id="process">Processing...</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-1.12.0.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>