# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a website created for my wife in their mystery clue-hunting game. The site contains screens that a user must enter a specific code to proceed to the next screen (which shows clues for the next screen)

### How do I get set up? ###

This is a PHP project so uploading to any server that supports it should work. There is not special modules required as these are just vanilla PHP codes.

### Contribution guidelines ###

You can use and modify the code to your own needs.

### Who do I talk to? ###

You do not need to ask permission from me to use the project, go ahead and clone.

I will not be changing anything for you so do not ask. Also, I am not liable if the project somehow causes problems at your end (which it shouldn't) so use at your own risk.