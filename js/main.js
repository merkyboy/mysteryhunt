$(function() 
{
	
	$('#txtScr1').focus();
	
	$('#btnScr1').on('click', function (){
		$('#screen1error').html('');
		$('#process').removeClass('hide');
		$('#btnScr1').blur();
		$.post( "process.php", {
			answer1: $('#txtScr1a').val(),
			answer2: $('#txtScr1b').val(),
			answer3: $('#txtScr1c').val(),
			answer4: $('#txtScr1d').val(),
			answer5: $('#txtScr1e').val(),
			answer6: $('#txtScr1f').val(),
			type: 1
		}, function( data ) {
			if (data != "fail") {
				
				setTimeout(function(){
					$('#process').addClass('hide');
					$('#screen1').addClass('hide');
					$('#screen2').html(data);
					$('#screen2').removeClass('hide');
				}, 1000);
				
			} else {
				$('#process').addClass('hide');
				$('#screen1error').html('How could you forget?');
			}
		});
	});

	$(document).on('click', '.screen2 #btnScr2', function (){
		$('#screen2error').html('');
		$('#process').removeClass('hide');
		$('#btnScr2').blur();
		$.post( "process.php", {
			answer1: $('#txtScr2a').val(),
			answer2: $('#txtScr2b').val(),
			answer3: $('#txtScr2c').val(),
			answer4: $('#txtScr2d').val(),
			answer5: $('#txtScr2e').val(),
			type: 2
		}, function( data ) {
			if (data != "fail") {
				
				setTimeout(function(){
					$('#process').addClass('hide');
					$('#screen2').addClass('hide');
					$('#screen3').html(data);
					$('#screen3').removeClass('hide');
				}, 1000);
				
			} else {
				$('#process').addClass('hide');
				$('#screen2error').html('That is not correct.');
			}
		});
	});

});